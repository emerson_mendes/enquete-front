import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { ConfigService } from './config.service';
import { Enquete, EnqueteCreate, EnqueteStatus } from '../enquete/enquete';
import { of } from 'rxjs/internal/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EnqueteService {
  
  constructor(private http: HttpClient, private config: ConfigService) { }
  
  /**
  * PEGA A LISTA DE ENQUETES
  */
  getEnquetes(): Observable<HttpResponse<Enquete[]>>{   
    return this.http.get<Enquete[]>(this.config.getEndPoint('/poll'), 
    {
      headers: this.config.getHeader(),
      observe: 'response',
      responseType: 'json'
    });
  }
  
  /**
  * PEGA A ENQUETE POR ID
  */
  getEnqueteById(id: number): Observable<HttpResponse<Enquete>>{   
    return this.http.get<Enquete>(this.config.getEndPoint('/poll/' + id), 
    {
      headers: this.config.getHeader(),
      observe: 'response',
      responseType: 'json'
    });
  }
  
  saveEnquete(enquete: EnqueteCreate): Observable<HttpResponse<any>>{
    return this.http.post<any>(this.config.getEndPoint('/poll'),
    enquete,
    {
      headers: this.config.getHeader(),
      observe: 'response',
      responseType: 'json'
    });
  }
  
  saveVote(enqueteId: number, vote: any): Observable<HttpResponse<any>>{
    return this.http.post<any>(this.config.getEndPoint('/poll/'+ enqueteId +'/vote	'),
    vote,
    {
      headers: this.config.getHeader(),
      observe: 'response',
      responseType: 'json'
    });
  }
  
  getEnqueteStatusById(id: number): Observable<HttpResponse<EnqueteStatus>>{   
    return this.http.get<EnqueteStatus>(this.config.getEndPoint('/poll/'+ id +'/stats'), 
    {
      headers: this.config.getHeader(),
      observe: 'response',
      responseType: 'json'
    });
  }
}
