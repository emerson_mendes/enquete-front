import { EnqueteService } from './../../../services/enquete.service';
import { Enquete, EnqueteOptions, EnqueteCreate } from './../../../enquete/enquete';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { PreloaderService } from '../../../services/preloader.service';
import { NotificationService } from '../../../services/notification.service';
import { HttpResponse } from '@angular/common/http';

declare var $: any;

@Component({
  selector: 'app-criar-enquete',
  templateUrl: './criar-enquete.component.html',
  styleUrls: ['./criar-enquete.component.css']
})
export class CriarEnqueteComponent implements OnInit {
  
  listOptions: Array<any> = [];
  enquete: EnqueteCreate;
  
  constructor(
    private preload: PreloaderService,
    private notification: NotificationService,
    private enq: EnqueteService
  ) { }
  
  ngOnInit() {
    $('.tooltipped').tooltip();
    this.addOptions();
    this.enquete = new EnqueteCreate('',this.listOptions);
    
  }
  
  // ADICINAR AS OPÇÕES
  addOptions(){
    this.listOptions.push({option_description: ''});
  }
  
  // REMOVE AS OPÇÕES
  removeOptions(index: number){
    this.listOptions.splice(index, 1);
  }
  
  // FORMATA O ARRAY MULTIPLO PARA SIMPLES ARRAY DE STRING
  formatArray(): Array<String>{
    const formatted: String[] = [];
    this.enquete.getOptions().forEach((value)=> { 
      formatted.push(value.option_description);
    });
    
    return formatted;
  }
  
  // RESETA OS VALORES DO FORM
  resetValues(enqueteForm: NgForm){
    this.listOptions = [];
    this.enquete = new EnqueteCreate('',this.listOptions);
    this.addOptions();
    enqueteForm.resetForm();
  }
  
  // FAZ O ENVIO DA NOVA ENQUETE SE OS CAMPOS FOREM VÁLIDOS
  onSubmit(enqueteForm: NgForm) {
    if(enqueteForm.valid){
      const saveValues =  new EnqueteCreate(this.enquete.getPoll_description(), this.formatArray());
      this.preload.preloaderOpen.emit(true);
      this.enq.saveEnquete(saveValues)
      .subscribe((res: HttpResponse<any>) => {
        if(res.status === 200){
          console.log(res.body.poll_id);
          this.notification.showNotification('Enquete salva com sucesso, no id ' + `${res.body.poll_id}` +'!', 'success', 10000);
          this.resetValues(enqueteForm);
        } else {
          this.notification.showNotification('Erro ' + `${res.status}` + ' ao buscar enquete', 'danger', 10000);
        }
        
        this.preload.preloaderOpen.emit(false);
      }, error => {         
        this.notification.showNotificationHttp(error, 'danger');
      });
    } else{
      this.notification.showNotification('Preencha todos os campos corretamente.', 'warning');
    }
  }
  
}
