import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PreloaderService {

  @Output() preloaderOpen: EventEmitter<any> = new EventEmitter();

  constructor() { }

}
