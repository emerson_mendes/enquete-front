import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VotarEnqueteComponent } from './votar-enquete.component';

describe('VotarEnqueteComponent', () => {
  let component: VotarEnqueteComponent;
  let fixture: ComponentFixture<VotarEnqueteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VotarEnqueteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VotarEnqueteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
