import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotificationService } from '../../../services/notification.service';
import { PreloaderService } from '../../../services/preloader.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EnqueteService } from '../../../services/enquete.service';
import { Enquete } from '../../../enquete/enquete';
import { Subscription } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { UtilService } from '../../../services/util.service';

@Component({
  selector: 'app-votar-enquete',
  templateUrl: './votar-enquete.component.html',
  styleUrls: ['./votar-enquete.component.css']
})
export class VotarEnqueteComponent implements OnInit, OnDestroy {
  
  enquete: Enquete;
  id: number;
  nextPage: number;
  subsc: Subscription;
  
  selectedOption: { [key: string]: any } = { option_id: null};
  
  constructor(
    private enq: EnqueteService, 
    private route: ActivatedRoute,
    private preload: PreloaderService,
    private notification: NotificationService,
    private router: Router,
    private util: UtilService
  ) { }
  
  ngOnInit() {
    this.subsc = this.route.params.subscribe(params => { 
      if(!isNaN(Number(params.id))) {
        this.id = Number(params.id); 
        this.nextPage = (Number(params.id) + 1);
      } else {
        this.nextPage = 1;
        this.id = 1;
      }
      this.getEnqueteById();
    });
  }
  
  //BUSCA ENQUETE POR ID
  getEnqueteById(): void {
    this.preload.preloaderOpen.emit(true);
    this.enq.getEnqueteById(this.id)
    .subscribe((res: HttpResponse<Enquete>) => {
      if(res.status === 200){
        this.enquete = res.body;
      } else {
        this.notification.showNotification('Erro ' + `${res.status}` + 'ao buscar enquete', 'danger');
      }
      
      this.preload.preloaderOpen.emit(false);
    }, error => this.util.handleError(error));
    
  }
  
  //VERIFICA O NUMERO MAXIMO DE ENQUETES
  checkMaxEnquetes(): boolean {
    if(this.id > Number(localStorage.getItem('SIZE'))) {
      return true;
    }
    return false;
  }
  
  ngOnDestroy(){
    if(this.subsc){ this.subsc.unsubscribe(); }
  }
  
  //ADICONA A ENQUETE VOTADA NA LISTA
  onSelectionChange(option) {    
    this.selectedOption = Object.assign({}, this.selectedOption, {'option_id':option});
  }
  
  //SALVA O VOTO
  onSubmit() {
    this.preload.preloaderOpen.emit(true);
    this.enq.saveVote(this.enquete.poll_id ,this.selectedOption)
    .subscribe((res: HttpResponse<any>) => {
      if(res.status === 200){
        this.onSelectionChange(null);
        this.notification.showNotification('Voto salvo sucesso, você será direcinado para próxima enquete.', 'success', 4000);
        if(!this.checkMaxEnquetes()){
          setTimeout(() => {
            this.router.navigate(['/enquete/votar/' + this.nextPage]);
          }, 2000);
        }else{
          setTimeout(() => {
            this.router.navigate(['/home']);
          }, 2000);
        }
       
      } else {
        this.notification.showNotification('Erro ' + `${res.status}` + ' ao salvar voto', 'danger', 10000);
      }
      
      this.preload.preloaderOpen.emit(false);
    }, error => this.util.handleError(error));
  }
  
}
