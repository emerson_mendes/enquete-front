import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

declare var $: any;
declare var M: any;

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private type = ['', 'info', 'success', 'warning', 'danger', 'primary'];

  constructor() { }

    /**
     * 
     * @param messageNotif 
     * @param type 
     * @param timerNotif 
     */
    showNotification(
      messageNotif: string,
      type = 'success',
      timerNotif = 4000
  ) {
    M.toast({
      html: messageNotif,
      classes: type,
      displayLength: timerNotif
    })
  }
    showNotificationHttp(
      response: HttpErrorResponse,
      type = 'success',
      timerNotif = 4000
  ) {
    M.toast({
      html: response.status + ' ' + response.statusText,
      classes: type,
      displayLength: timerNotif
    })
  }
}
