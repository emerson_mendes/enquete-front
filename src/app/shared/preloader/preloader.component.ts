import { Component, OnInit, ViewChild, ElementRef, Renderer, OnDestroy } from '@angular/core';
import { PreloaderService } from '../../services/preloader.service';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'app-preloader',
  templateUrl: './preloader.component.html',
  styleUrls: ['./preloader.component.css']
})
export class PreloaderComponent implements OnDestroy {

  @ViewChild('preloader') preloader: ElementRef;

  private subscript: Subscription;

  constructor(private el: ElementRef, private renderer: Renderer, private preload: PreloaderService) {
    this.subscript = this.preload.preloaderOpen.subscribe((val) => {
      if (val) {
        this.preloader.nativeElement.style.display = 'block';
      } else {
        this.preloader.nativeElement.style.display = 'none';
      }
    });
  }

  ngOnDestroy(){
    if(this.subscript){ this.subscript.unsubscribe() }
  }

}
