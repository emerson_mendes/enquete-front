# CONFIGURAÇÕES PARA LINUX

## APACHE
sudo apt-get install apache2

service apache2 restart

sudo a2enmod rewrite

nano /etc/apache2/sites-available/000-default.conf 

```apache
<VirtualHost *:80>
  ServerAdmin webmaster@localhost 
  DocumentRoot /var/www/enquetefront/dist 
  ErrorLog ${APACHE_LOG_DIR}/error.log 
  CustomLog ${APACHE_LOG_DIR}/access.log combined 
</VirtualHost>
```

nano /etc/apache2/apache2.conf 

ALTERAR  **AllowOverride none** PARA  **AllowOverride All**

```apache
<Directory /var/www/>
  Options Indexes FollowSymLinks
  AllowOverride All
  Require all granted
</Directory>
```

service apache2 restart

## NODEJS
sudo apt-get install curl

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash 

sudo apt-get install nodejs

node -v

## ANGULAR-CLI
npm install -g @angular/cli@latest

ng -v

## GIT
cd /var/www

git clone https://emerson_mendes@bitbucket.org/emerson_mendes/enquete-front.git enquetefront

## GERAR BUILD
cd /var/www/enquetefront

npm install

ng build --prod 

cd /var/www/enquetefront/dist/enquete-front

cp -R * ..

cd ..

rm -R enquete-front

touch .htaccess

nano .htaccess

COLAR DENTRO DO .htaccess

```.htaccess
<IfModule mod_rewrite.c>
    RewriteEngine on
    RewriteCond %{REQUEST_FILENAME} -s [OR]
    RewriteCond %{REQUEST_FILENAME} -l [OR]
    RewriteCond %{REQUEST_FILENAME} -d
    RewriteRule ^.*$ - [NC,L]
    RewriteRule ^(.*) index.html [NC,L]
</IfModule>
```

service apache2 restart

PRONTO


# CONFIGURÇÕES LOCAIS EnqueteFront 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

