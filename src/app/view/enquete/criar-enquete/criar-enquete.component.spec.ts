import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CriarEnqueteComponent } from './criar-enquete.component';

describe('CriarEnqueteComponent', () => {
  let component: CriarEnqueteComponent;
  let fixture: ComponentFixture<CriarEnqueteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CriarEnqueteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CriarEnqueteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
