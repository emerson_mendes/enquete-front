import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusEnqueteComponent } from './status-enquete.component';

describe('StatusEnqueteComponent', () => {
  let component: StatusEnqueteComponent;
  let fixture: ComponentFixture<StatusEnqueteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusEnqueteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusEnqueteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
