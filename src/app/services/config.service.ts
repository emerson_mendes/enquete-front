import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private headers: HttpHeaders;
  private apiUrl = 'http://enqueteapi.luxfacta.com';

  constructor() { }

  /**
   * CRIA E SETA OS HEADERS
   */
  public getHeader() {
   
    this.headers = new HttpHeaders();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Content-Type', 'application/x-www-form-urlencoded');

    return this.headers;
  }

  /**
   * PEGA AS CONFIGURAÇÕES DO ENDPOINT E RETORNA COM OS PARAMENTROS PASSADOS
   * 
   * @param resource 
   */
  public getEndPoint(resource: string){
    return this.apiUrl + resource;;
  }
}
