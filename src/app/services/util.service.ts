import { Injectable } from '@angular/core';
import { NotificationService } from './notification.service';
import { PreloaderService } from './preloader.service';

@Injectable({
  providedIn: 'root'
})
export class UtilService {
  
  constructor(
    private notification: NotificationService,
    private preload: PreloaderService,
  ) { }
  
  handleError(error: any): void {
    this.preload.preloaderOpen.emit(false);
    this.notification.showNotificationHttp(error, 'danger')
  }
}
