import { VotarEnqueteComponent } from './view/enquete/votar-enquete/votar-enquete.component';
import { CriarEnqueteComponent } from './view/enquete/criar-enquete/criar-enquete.component';
import { HomeComponent } from './view/home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StatusEnqueteComponent } from './view/enquete/status-enquete/status-enquete.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'enquete/new', component: CriarEnqueteComponent },
  { path: 'enquete/votar/:id', component: VotarEnqueteComponent },
  { path: 'enquete/status/:id', component: StatusEnqueteComponent },
  { path: '**', redirectTo: '/home' }
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
