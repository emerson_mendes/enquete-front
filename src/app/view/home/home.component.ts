import { Component, OnInit } from '@angular/core';
import { Enquete } from '../../enquete/enquete';
import { EnqueteService } from '../../services/enquete.service';
import { HttpResponse } from '@angular/common/http';
import { PreloaderService } from '../../services/preloader.service';
import { UtilService } from '../../services/util.service';

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements  OnInit{
  
  enquetes: Enquete[];

  constructor(
    private enq: EnqueteService, 
    private preload: PreloaderService,
    private util: UtilService
  ){

  }

  ngOnInit(){    
    $('.collapsible').collapsible();
    this.getEnquetes()
  }

  getEnquetes(): void {
    this.preload.preloaderOpen.emit(true);
    this.enq.getEnquetes()
    .subscribe((res: HttpResponse<Enquete[]>) => {
      if(res.status === 200){
       this.enquetes = res.body;
       localStorage.setItem('SIZE',this.enquetes.length.toString());
      }
      this.preload.preloaderOpen.emit(false);
    }, error => this.util.handleError(error));
  }
 
}