export class Enquete {
  poll_description: string;
  poll_id: number;
  options: EnqueteOptions[];
}

export class EnqueteOptions {
  option_id: number;
  option_description: string;
}

export class EnqueteCreate {
  poll_description: string;
  options: Array<any>;
  
  constructor(poll_description: string, options: Array<any>){
    this.poll_description = poll_description;
    this.options = options;
  }
  
  public getPoll_description() {
    return this.poll_description;
  }
  
  public setPoll_description(poll_description: string) {
    this.poll_description = poll_description;
  }
  
  public getOptions(){
    return this.options;
  }
  
  public setOptions(options: Array<any>){
    this.options = options;
  }
}

export class EnqueteStatus{
  poll_id: number;
  views: number;
  votes: [{option_id: number, qty: number}];
}