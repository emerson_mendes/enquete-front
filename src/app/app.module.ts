
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { PreloaderComponent } from './shared/preloader/preloader.component';
import { HomeComponent } from './view/home/home.component';
import { AppRoutingModule } from './/app-routing.module';
import { CriarEnqueteComponent } from './view/enquete/criar-enquete/criar-enquete.component';
import { VotarEnqueteComponent } from './view/enquete/votar-enquete/votar-enquete.component';
import { StatusEnqueteComponent } from './view/enquete/status-enquete/status-enquete.component';

@NgModule({
  declarations: [
    AppComponent,
    PreloaderComponent,
    HomeComponent,
    CriarEnqueteComponent,
    VotarEnqueteComponent,
    StatusEnqueteComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
