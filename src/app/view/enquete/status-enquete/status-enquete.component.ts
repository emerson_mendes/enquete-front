import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { PreloaderService } from '../../../services/preloader.service';
import { NotificationService } from '../../../services/notification.service';
import { EnqueteService } from '../../../services/enquete.service';
import { HttpResponse } from '@angular/common/http';
import { EnqueteStatus, Enquete } from '../../../enquete/enquete';
import { UtilService } from '../../../services/util.service';

@Component({
  selector: 'app-status-enquete',
  templateUrl: './status-enquete.component.html',
  styleUrls: ['./status-enquete.component.css']
})
export class StatusEnqueteComponent implements OnInit, OnDestroy {
  
  subsc: Subscription;
  id: number;
  nextPage: number;
  beforePage: number;
  enqueteStatus: EnqueteStatus;
  enquete: Enquete;
  porcentagemNulo: Number = 0;
  
  constructor(
    private route: ActivatedRoute,
    private preload: PreloaderService,
    private notification: NotificationService,
    private enq: EnqueteService,
    private util: UtilService
  ) { }
  
  ngOnInit() {
    this.subsc = this.route.params.subscribe(params => { 
      if(!isNaN(Number(params.id))) {
        this.id = Number(params.id); 
        this.nextPage = (Number(params.id) + 1);
        this.beforePage = Number(params.id) == 1 ? 1 : (Number(params.id) - 1);
      } else {
        this.nextPage = 1;
        this.beforePage = 1;
        this.id = 1;
      }
      this.enqueteStatus = null;
      this.enquete = null;
      
      this.getEnqueteById();
    });
  }
  
  //FAZ O CALCULO DA PORCENTAGEM VOTOS
  calcularPorcentagem(param: number): Number{
    let calc: number = 0;
    calc =  Math.round(Number((Number(param) / Number(this.enqueteStatus.views)) * Number(100))); 
    return calc;
  }
  
  //FAZ O CALCULO DA PORCENTAGEM DE NULOS
  calcularPorcentagemNulo(){
    let calc: number = 0;
    let totalViews: number = this.enqueteStatus.views;
    let totalVotos: number = 0;

    this.enqueteStatus.votes.forEach((res) => {
      totalVotos = Number(totalVotos) + Number(res.qty);
    });
    
   this.porcentagemNulo = this.calcularPorcentagem(totalViews - totalVotos); 

  }
  
  //BUSCA A ENQUETE PELO ID
  getEnqueteById(): void {
    this.preload.preloaderOpen.emit(true);
    this.enq.getEnqueteById(this.id)
    .subscribe((res: HttpResponse<Enquete>) => {
      if(res.status === 200){
        this.enquete = res.body;
        this.getEnqueteStatusById();
      } else {
        this.notification.showNotification('Erro ' + `${res.status}` + 'ao buscar enquete', 'danger');
        this.preload.preloaderOpen.emit(false);
      }
    }, error => this.util.handleError(error));
    
  }
  
  //BUSCA O STATUS PELO ID
  getEnqueteStatusById(): void {
    this.enq.getEnqueteStatusById(this.id)
    .subscribe((res: HttpResponse<EnqueteStatus>) => {
      if(res.status === 200){
        this.enqueteStatus = res.body;
        this.calcularPorcentagemNulo();
      } else {
        this.notification.showNotification('Erro ' + `${res.status}` + 'ao buscar status da enquete', 'danger');
      }
      
      this.preload.preloaderOpen.emit(false);
    }, error => this.util.handleError(error));
  }
  
  //VERIFICA SE TEM PRIMA ENQUETE
  checkMaxEnquetes(): boolean {
    if(this.id > Number(localStorage.getItem('SIZE'))) {
      return true;
    }
    return false;
  }
  
  //VERIFICA SE TEM ENQUETES PARA VOLTAR
  checkMinEnquetes(): boolean {
    if(this.id <= 1) {
      return true;
    }
    return false;
  }
  
  ngOnDestroy(){
    if(this.subsc){this.subsc.unsubscribe()}
  }
}
